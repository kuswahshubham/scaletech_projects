from flask import Flask, redirect, url_for, request,render_template
from flask import flash,session,abort
from flask_paginate import Pagination, get_page_args
import json
import os
import datetime
import mysql.connector

#connecting database
mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      passwd="",
      database="tododatabase"
)

app = Flask(__name__)

#The route() function of the Flask class is a decorator,
# which tells the application which URL should call the associated function
#‘/’ URL is bound with hello_world() function. Hence,
# when the home page of web server is opened in browser,
# the output of this function will be rendered
######################################################################
######################Admin Tasks#####################################
######################################################################
#admin panel for admin ,displaying user data here
@app.route('/admin')
def admin():
   mycursor = mydb.cursor()
   mycursor.execute("select table_user.user_id,table_user.user_name,table_user.user_role,table_user.user_state,count(table_todo.user_id)"
                    "FROM table_user "
                    "LEFT JOIN table_todo ON table_user.user_id=table_todo.user_id GROUP BY table_user.user_id ORDER BY count(table_todo.user_id) DESC")
   #mycursor.execute("select * from table_user where user_id !=3")
   data = mycursor.fetchall()#returns result query of type list
   mydb.commit()
   #data.sort()
   return render_template("adminpanel.html",val=data)


##pagination code###
users = list(range(100))


def get_users(offset=0, per_page=10):
    return users[offset: offset + per_page]
##pagination code###

#search user by the task title and description through the search bar
@app.route('/adminpanelsearch',)
def adminpanelsearch():
    #query=request.form['searchquery']
    mycursor = mydb.cursor()
    #query = "bill"
    query = str(request.args.get('searchquery'))
    print(request.args.get('searchquery'))
    print(type(request.args.get('searchquery')))

    mycursor.execute("SELECT table_user.*,"
                     "table_todo.* "
                     "FROM table_todo "
                     "LEFT JOIN table_user "
                     "ON table_todo.user_id=table_user.user_id "
                     "where table_todo.todo_title LIKE '%" + query + "%' "
                     "or table_todo.todo_description LIKE '%" + query + "%'"
                 )


    data = mycursor.fetchall()
    print(data)
    mydb.commit()

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')
    total = len(data)
    # total=len(data)
    # print(total)
    pagination_users = get_users(offset=offset, per_page=10)
    pagination = Pagination(page=page, per_page=10, total=total,
                            css_framework='bootstrap4')

    return render_template('adminpanelsearch.html',
                           users=pagination_users,
                           page=page,
                           per_page=per_page,
                           pagination=pagination,
                           result=data,
                           total=total
                           )

#redirecting admin to new page to enter new user data
@app.route('/addnewuser',methods=['POST'])
def adduser():
    return render_template('AddNewUser.html')

#admin can view the specific user's todo by using his id
@app.route('/viewusertodo/<id>',methods=['POST'])
def viewusertodo(id):
    session['user_id']=id
    return index()

#admin can change the suspend state of user which disable the login of perticular user
#untill admin unsuspend the user.
@app.route('/suspenduser/<id>',methods=['POST'])
def suspenduser(id):
    mycursor=mydb.cursor()
    mycursor.execute("select user_state from table_user where user_id='"+id+"'")
    data=mycursor.fetchone()
    userstate=data[0]
    #mycursor.execute("UPDATE table_user SET user_state=0 WHERE user_id='"+id+"'")
    #mydb.commit()
    print (userstate)
    #changin the userstate here..............0=suspend and 1=active
    if userstate==0:
        mycursor.execute("UPDATE table_user SET user_state=1 WHERE user_id='" + id + "'")
        mydb.commit()
    else:
        mycursor.execute("UPDATE table_user SET user_state=0 WHERE user_id='" + id + "'")
        mydb.commit()

    return redirect(url_for('admin'))

#delete the user by using his user_id
@app.route('/deleteuser/<id>',methods=['POST'])
def deleteuser(id):
    mycursor=mydb.cursor()
    mycursor.execute("DELETE FROM table_user where user_id='"+id+"'")
    mydb.commit()
    mycursor.execute("DELETE FROM table_todo where user_id='" + id + "'")
    mydb.commit()
    return redirect(url_for('admin'))

#adding new user to the database
@app.route('/userAdd',methods=['POST'])
def userAdd():
   if request.form['addsubmit']=="Cancel":
      return redirect(url_for('admin'))
   if request.form['addsubmit']=="Add New User":
      username=request.form['username']
      userpassword=request.form['userpassword']
      userrole=request.form['userrole']
      mycursor=mydb.cursor()
      sql="INSERT INTO table_user (user_name,user_password,user_role)" \
          "VALUES (%s,%s,%s)"
      val=(username,userpassword,userrole)
      mycursor.execute(sql,val)
      mydb.commit()


      return redirect(url_for('admin'))

@app.route('/search', methods=['POST'])
def searchUser():
    print(request.data)
    return "hi"

######################################################################
######################User side Tasks#####################################
######################################################################

#home page for any logged in user and displaying all the tasks by default.
@app.route('/')
def index():
    if not session.get('user_id'):
        return redirect(url_for('login'))
    else:
       if session['user_id']=="3":
          return admin()
       else:
          mycursor = mydb.cursor()
          mycursor.execute("select * from table_todo where user_id='"+session['user_id']+"'")
          data = mycursor.fetchall()

          for row in data:
             print(row[1], "  ", row[2], "  ", row[3], "  ", row[4])

          mydb.commit()

          return render_template("index.html",result=data)

#login validation for user and admin
@app.route('/login')
def login():
   if not session.get('user_id'):
      return render_template('login.html')
   else:
      return index()

#validating user by the username and password he enters
@app.route('/validate',methods=['POST','GET'])
def user_validate():
   mycursor = mydb.cursor()
   username=request.form['username']
   userpassword=request.form['password']

   mycursor.execute("select * from table_user where user_name='"+username+"' and user_password='"+userpassword+"'")
   data=mycursor.fetchone()


   if data is None: #if no data found then return to login page
      flash('wrong username or password!')
      return redirect(url_for('login'))
   else:
      mycursor.execute("SELECT user_id FROM table_user WHERE user_name = '" + username + "'")
      a = mycursor.fetchone() #return a tuble containing user_id of logged in user
      b = a[0]
      c = str(b)
      session['user_id'] = c #storign user in in session
      mycursor.execute("select user_state from table_user where user_id='" + c + "'")
      data = mycursor.fetchone()
      userstate = data[0]
      #let the user login in only if he/she is not suspended.
      if userstate==1:
          session['user_id'] = c
          return index()
      else:
          return redirect('login')


   """
   if str(request.form['password']) in user_passwords or (request.form['username']) in user_names:
      #session['logged_in'] = True
      print ("true")
      return index()
   else:
      flash('wrong password!')
   """
   #return login()

   #return render_template('login.html')
#setting the session to null and return to the login page.
@app.route("/logout")
def logout():
   session.pop('user_id',None)
   return redirect(url_for('login'))



@app.route('/temp/<id>',methods=['post'])
def temp(id):
    return id

#user can add task here.....
@app.route('/addtask',methods = ['POST'])
def addtask():
    taskdate = request.form['taskdate']
    tasktime = request.form['tasktime']
    tasktitle = request.form['tasktitle']
    taskdescription = request.form['taskdescription']
    mycursor = mydb.cursor()
    # code to insert the data into table according to the session of user
    sql = "INSERT INTO table_todo (todo_title,todo_description,todo_date,todo_time,user_id) " \
          "VALUES (%s,%s,%s,%s,%s)"
    val = (tasktitle, taskdescription, taskdate, tasktime, session['user_id'])

    mycursor.execute(sql, val)
    mycursor.execute("select * from table_todo where user_id='" + session['user_id'] + "'")
    data = mycursor.fetchall()

    for row in data:
       print(row[1], "  ", row[2], "  ", row[3], "  ", row[4])

    mydb.commit()
    return render_template("index.html",result=data)

#deleting the task according to the todo_id
@app.route('/deletetask/<id>',methods=['post'])
def deletetask(id):
    mycursor=mydb.cursor()
    mycursor.execute("delete  from table_todo WHERE todo_id='"+id+"'")
    mydb.commit()
    return index()
    #return redirect(url_for('/'))

#redirecting user to updatetask.html page to update the task
@app.route('/updatetask/<id>',methods=['post'])
def updatetask(id):
    mycursor=mydb.cursor()
    mycursor.execute("select * from table_todo where todo_id='"+id+"'")
    val=mycursor.fetchall()
    return render_template('updatetask.html',data=val)
    #return redirect(url_for('/'))


#updating the task according to the todo_id of task
@app.route('/update/<id>',methods=['post'])
def update(id):
    mycursor=mydb.cursor()
    taskdate = request.form['taskdate']
    tasktime = request.form['tasktime']
    tasktitle = request.form['tasktitle']
    taskdescription = request.form['taskdescription']
    mycursor.execute("UPDATE  table_todo "
                     "SET todo_title='"+tasktitle+"' ,todo_description='"+taskdescription+"' ,"
                     "todo_date='"+taskdate+"' ,todo_time='"+tasktime+"'   "
                     "where "
                     "todo_id='"+id+"'")


    mydb.commit()
    return index()


#main method that runs the program
if __name__ == '__main__':
   #app.run(host, port, debug, options)
   app.secret_key = os.urandom(12)
   app.debug = True
   app.run()
   app.run(debug=True)



"""
      #code to update the table with inserted data
      mydb.commit()
      mycursor.execute("select * from table_todo")
      data = mycursor.fetchall()
      mydb.commit()
      return render_template('index.html',val=data)
         """




#####json code to retrieve data of user
@app.route('/getTaskList',methods = ['POST', 'GET'])
def getTaskList():
   if request.method == 'GET':
      mycursor = mydb.cursor()
      mycursor.execute("select * from table_todo")
      data = mycursor.fetchall()

      def jsonConverter(o):
         if isinstance(o, datetime.date) or isinstance(o, datetime.timedelta):
            return o.__str__()

      # for row in data:
      #    print(row[1], "  ", row[2], "  ", row[3], "  ", row[4])
      print(type(data))

      return json.dumps(data,default=jsonConverter)

"""
@app.route('/admintask/<id>',methods=['POST','GET'])
def admintask(id):
   if request.form['bsubmit']=="Add New User":
      return render_template('AddNewUser.html')
   if request.form['bsubmit']== "View Todos":
      return id
   if request.form['bsubmit']== "Suspend User":
      return id
   if request.form['bsubmit']== "Delete User":
      return id"""
