import json
import time,random,string

#funciton to generate random fixed size token for user
def token_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

#generating variables
user_id=311

#print(user_id)
#print(timestamp)
#print(game_id)
#print(user_token)
#print(operation)
#print(type(operation))

data={}
data['user_info']=[]

#preparing data for json file--Nested dictionary
for i in range(10):
    timestamp = time.time()
    game_id = random.randint(1, 2)
    user_token = token_generator(32)
    choices = random.choices(['opened', 'spined'])
    operation = "".join(choices)
    data['user_info'].append(
        {"user_token": user_token,
         "user_id": user_id,
         "timestamp": timestamp,
         "game_id": game_id,
         "user_operation":operation})

#writing data to json file
"""
with open("GameData.json","w") as outputfile:
    json.dump(data,outputfile,indent=4)
    outputfile.write("\n")
outputfile.close()
"""

#####Reading json file here
with open("GameData.json") as f:
    data=json.load(f)

print(len(data['user_info']))
for i in range(len(data['user_info'])):
    print (i,data['user_info'][i]['user_id'],data['user_info'][i]['user_token'],
           data['user_info'][i]['timestamp'],data['user_info'][i]['game_id'],
           data['user_info'][i]['user_operation'])


#for x in data:
    #print("%s : %d" % (x,data[x]))
    #print(type(x))
"""

fh=open("GameData.json","a")

for i in range(10):
    fh.write(json.dumps({"User_info"+str(i):[{"user_token":user_token,
                         "user_id":user_id,
                         "timestamp":timestamp,
                         "game_id":game_id,
                         "user_operation":operation}]},indent=4))
    fh.write("\n")

fh.close()
"""