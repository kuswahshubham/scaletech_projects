const request = require('request');
//yargs to take input from the command line.
//run program like "node index.js -c city_name"
const argv = require('yargs').argv;


let apiKey = 'bbfc0331263a1a65008ea1863e6da099';
let city = argv.c || 'ahmedabad';
//units=metric for celsius and if you use Fahrenheit you’d use units=imperial.
let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`

request(url, function (err, response, body) {
  if(err){
    console.log('error:', error);
  } else {
//    console.log('body:', body);
    let weather=JSON.parse(body)

    let message = `It's ${weather.main.temp} celsius in ${weather.name}!`;

    console.log(message);

  }
});
