const express = require('express')
const request = require('request');
const app = express()
const bodyParser=require('body-parser')

let apiKey = 'bbfc0331263a1a65008ea1863e6da099';

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static('public'));
app.set('view engine', 'ejs')


app.get('/', function (req, res) {
  //res.send('Hello World!')
  res.render('index')
})

app.post('/', function (req, res) {
  let city=req.body.city
  let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`

  request(url, function (err, response, body) {
    if(err){
      res.render('index', {weather: null, error: 'Error, please try again'});
    } else {
      let weather = JSON.parse(body)
      if(weather.main == undefined){
        res.render('index', {weather: null, error: 'Error, please try again'});
      } else {
        let weatherText = `It's ${weather.main.temp} degrees in ${weather.name}!`;
        res.render('index', {weather: weatherText, error: null});
      }
    }
  });
  //res.send('Hello World!')
  //res.render('index')
  //console.log(req.body.city);
})

app.listen(3000, function () {
  console.log('app listening on port 3000!')
})
