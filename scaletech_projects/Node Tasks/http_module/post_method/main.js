//including necessary modules
const http = require('http');
const { parse } = require('querystring');

const server = http.createServer((req, res) => {
  //when user submit the form
    if (req.method === 'POST') {
      //calling collectRequestData method to handle request and responsse.
        collectRequestData(req, result => {
            console.log(result);

            res.write(`your name is: ${result.fname}   `)


            res.write(`your age is ${result.age}`)

            //res.end(`Parsed data belonging to ${result.fname}`);
            res.end()
        });
    }
    else {
      //if not post method then display html page to user
      //by default when user open local server display below html page
        res.end(`
            <!doctype html>
            <html>
            <body>
                <form action="/" method="post">
                <table align='center' >
                <tr rowspan="2">
                <th>Send data through post method
                </th><th></th>
                </tr>

                    <tr>
                    <td>Name</td>
                    <td><input type="text" name="fname" /><br /></td>
                    </tr>
                    <tr>
                    <td>Age</td>
                    <td><input type="number" name="age" /><br /></td>

                    <tr colspan="2"><td><button>Save</button><td> <td></td></tr>
                </table>
                </form>
            </body>
            </html>
        `);
    }
});
server.listen(3000);
//function to collect the data submitted by user
function collectRequestData(request, callback) {
    const FORM_URLENCODED = 'application/x-www-form-urlencoded';
    if(request.headers['content-type'] === FORM_URLENCODED) {
        let body = '';
        request.on('data', chunk => {
            body += chunk.toString();
        });
        request.on('end', () => {
            callback(parse(body));
        });
    }
    else {
        callback(null);
    }
}
