// Include http ,fs and url module.
var http = require('http');
var fs = require('fs');
var url = require('url');

// Create http server.
var httpServer = http.createServer(function (req, resp) {

    // Get an parse client request url.
    var reqUrlString = req.url;
    var urlObject = url.parse(reqUrlString, true, false);

    // Get user request file name.
    var fileName = urlObject.pathname;
    fileName = fileName.substr(1);

    // Read the file content and return to client when read complete.
    fs.readFile(fileName, {encoding:'utf-8', flag:'r'}, function (error, data) {

        if(!error)
        {
          //if no error in fetching file send response back to user containing data.
            resp.writeHead(200, {'Access-Control-Allow-Origin':'*'});
            resp.end(data);
        }else
        {
            resp.writeHead(404, {'Access-Control-Allow-Origin':'*'});
            resp.end(JSON.stringify(error));
        }
    });
});

// Http server listen on port 8888.
httpServer.listen(8888);

console.log("listening")
