var express = require('express');
var router = express.Router();
var bodyParser=require('body-parser');
var path = require('path');
var urlencodedParser = bodyParser.urlencoded({ extended: true });


//__dirname='express_module';
router.get('/index', function(req, res) {
  console.log('index');
  //res.send('home page');
  //res.sendFile('../html files/index.html, {root: __dirname}');
  //res.sendFile(path.join(__dirname, '../html files', 'index.html'));
  //console.log(path.resolve('./routes'));
  res.sendFile(path.resolve(`html files/index.html`));

});

router.get('/home', function(req, res) {
  res.send('login page');
});

router.post('/data',urlencodedParser,function(req,res){
  //getting user data in variables.
  console.log('getting data')
  console.log(req.body);


  //res.render('/data',{data:req.body})

  var user_name=req.body.username;
  var user_age=req.body.age;
  //res.write(user_name);
  data='';
  data+='your name is :'+user_name;
  data+='  your age is :'+user_age;
  res.send(data)
  //console.log(user_name);
  //printing user data to console .
  //console.log("User name = "+user_name+", age is "+user_age);
  res.end();
});

module.exports = router
