const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const path=require('path')
const express=require('express')
const router=express.Router()
const db=require('../models/mylife.database')
const responseGenerator = require('../response/response-generator');

const Events = require('../models/mylife.model');
const User=require('../models/mylife.user')

exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.event_create=function(req,res){
    //var token = window.localStorage.getItem('token');
    let event=new Events(
      {
        user_id:req.body.user_id,
        event_date:req.body.event_date,
        event:req.body.event
      })
    event.save()
      .then(doc => {
     console.log(doc)
      })
      .catch(err => {
     console.error(err)
      })
      res.send('event created successfully')
}

exports.event_read=function(req,res){

    Events.find({event_id:req.params.id},function(err,event,next){
      if(err){
        return next(err)
      }
      res.send(event)
    })
}

exports.event_readall=function(req,res){

    Events.find(function(err,event,next){
      if(err){
        return next(err)
      }
      res.send(event)
    })
}


exports.event_delete=function(req,res){
  Events.deleteOne({event_id:req.params.id},function(err,event,next){
    if(err){
      //return next(err)
    }
    res.send("deleted successfully")
  })
}


exports.event_deleteuser=function(req,res){
      User.deleteOne({name:req.params.id},function(err,event,next){
        if(err){
            console.log(err)
        }
        res.send("user deleted")
      })
}
exports.event_deleteall=function(req,res){
  Events.deleteMany(function(err,event,next){
    if(err){
      return next(err)
    }
    res.send("All records deleted!!!")
  })
}

exports.event_update = function(req,res) {
  console.log("hereeeeee", req);
  Events.updateOne({event_id:req.params.id},{$set:{event:req.body.event}}
    ,function(err,event,next){
    if(err) return next(err)
    res.send("event updated")
  })
}

exports.event_createuser=function(req,res){
  let username=req.body.username
  let password=req.body.password
  let userrole=req.body.role
  let userid=req.body.userid

  let newuser=new User()
  newuser.name= username
  newuser.password= password
  newuser.role= userrole
  newuser.user_id=userid

  newuser.save(function(err,saveduser){
    if(err){
      res.send(err)

    }
    return res.send("user created successfully")
  })
}

exports.event_userlist=function(req,res){

  User.find({},function(err,users,next){
    if(err){
      console.log(err)
      return next(err)
    }
    res.send(users)
  })
}
exports.event_login=function(req,res){
  let username=req.body.username
  let password=req.body.password

  User.find({name:username,password:password}, (err, data) =>{
    if(err){
      console.log(err)
      res.send("something went wrong")
    }
    if (data.length<1)
    {
      res.send("username or password does not match")
    }
    //res.send(data)
    res.send(data[0].role)
  });
}

exports.event_authenticate = function(req,res,next){

  console.log('asdas');
  let username=req.body.username
  let password=req.body.password

  User.find({name:username}, (err, data) =>{
    if(err){
      console.log(err)
      res.send("something went wrong")
      next(err)
    }
    if (data.length<1)
    {
      //res.send("user does not found")
      // res.sendFile(path.resolve(`views/login.html`))
       res.redirect('/');
    }
    else{
      if(bcrypt.compareSync(password,data[0].password)){
        const token= jwt.sign({user_id:data[0].user_id,role: data[0].role},'secretKey')
        // window.localStorage.setItem('my_token', token)
        res.send({'my_token': token});
        // res.send({})
      }
      else{
        res.redirect('/login')
      }
    }

  });
}

exports.event_userdata=function(req,res){

  Events.find({event_id:req.params.id},function(err,event,next){
    if(err){
      return next(err)
    }
    res.send(event)
  })
}
