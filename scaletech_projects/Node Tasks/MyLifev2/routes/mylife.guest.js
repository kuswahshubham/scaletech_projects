const express=require('express')
const path=require('path')
var bodyParser=require('body-parser');
const router=express.Router()
const validateUser=require('../middleware/validation')
const mylife_controller=require('../controllers/mylife.controller')


router.post('/create',mylife_controller.event_create)
router.get('/read',mylife_controller.event_readall)
router.get('/read/:id',mylife_controller.event_read)

module.exports=router
