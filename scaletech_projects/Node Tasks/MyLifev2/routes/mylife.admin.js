const express=require('express')
const path=require('path')
var bodyParser=require('body-parser');
const router=express.Router()
//const validateUser=require('../middleware/validation')
const validateUser=require('../middleware/Authentication')
const mylife_controller=require('../controllers/mylife.controller')


router.put('/update/:id',mylife_controller.event_update)
router.delete('/delete',mylife_controller.event_deleteall)
router.delete('/delete/:id',mylife_controller.event_delete)


router.get('/showusers',mylife_controller.event_userlist)
router.post('/createuser',mylife_controller.event_createuser)
router.post('/deleteuser',mylife_controller.event_deleteuser)

module.exports=router
