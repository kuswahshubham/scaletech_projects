const express=require('express')
const path=require('path')
var bodyParser=require('body-parser');
const router=express.Router()
const validateUser=require('../middleware/validation')
const mylife_controller=require('../controllers/mylife.controller')
const vtoken=require('../middleware/Authentication')


console.log(mylife_controller)

router.get('/test',mylife_controller.test)
router.get('/login',function(req,res){res.sendFile(path.resolve(`views/login.html`))})
router.get('/testtoken',vtoken.verifyGuest)
router.post('/authenticate',mylife_controller.event_authenticate)
router.get('/admin',mylife_controller.event_userlist)
router.get('/guest',mylife_controller.event_userdata)
/*
router.get('/index',function(req,res){res.sendFile(path.resolve('views/index.html'))})
router.get('/test',mylife_controller.test)

//////CRUD operations for admin and user to manage events///////////
router.post('/create',mylife_controller.event_create)
router.get('/read',mylife_controller.event_readall)
//router.get('/read/:id',mylife_controller.event_read)
router.put('/update/:id',validateUser.validate,mylife_controller.event_update)
router.delete('/delete',mylife_controller.event_deleteall)
router.delete('/delete/:id',mylife_controller.event_delete)


///routes for admin to create new user and view userss/////
router.get('/showusers',mylife_controller.event_userlist)
router.post('/createuser',mylife_controller.event_createuser)
router.post('/deleteuser',mylife_controller.event_deleteuser)

////route to check user login
//router.post('/login',mylife_controller.event_login)


*/
module.exports=router
