let mongoose=require('mongoose')
mongoose.set('useCreateIndex', true);
//const server='localhost:27017'
//const database='mylife_db'
var url = "mongodb://localhost:27017/mylife_db";
class Database{
  async connect(){
    mongoose.connect(url,{useNewUrlParser:true} )
    .then(()=>{
       console.log("database connection successfull")
          })
    .catch(err=>{
        console.log("database connection error")
      })
  }
}

module.exports=new Database();
