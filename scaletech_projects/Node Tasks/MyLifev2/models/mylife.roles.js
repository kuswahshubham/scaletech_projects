let roles={
  admin:{
    can:['update','delete'],
    inherits:['guest']
  },
  guest:{
    can:['create','read']
  }
}

function can(role,operation){
  return roles[role] && roles[role].can.indexOf(operation) !== -1;
}
