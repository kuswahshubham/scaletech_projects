const mongoose=require('mongoose')
const Schema=mongoose.Schema

let mylifeSchema2=new Schema(
  {
    user_id:{
      type:Number,
      required:true,
      trime:true
    },
    event_date:{
      type:Date,
      default: Date.now,
      required:true,
      unique:false,
      trim:true
    },
    event:{
      type:String,
      required:true,
      unique:false,
      lowercase:true,
      max:50,
      trim:true
    }
  }
);

module.exports=mongoose.model('test',mylifeSchema2)
