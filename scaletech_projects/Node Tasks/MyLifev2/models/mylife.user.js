const mongoose=require('mongoose')
const bcrypt=require('bcrypt')
const saltRounds=10
const UserSchema=mongoose.Schema

let userschema=new UserSchema(
  {

    name:{
      type:String,
      required:true,
      minlength:5,
      maxlength:50,
      trim:true
    },
    password:{
      type:String,
      required:true,
      minlength:3,
      maxlength:20,
      trim:true
    },
    role:{
      type:String,
      required:true,
      trim:true
    },
    user_id:{
      type:Number,
      required:true,
      trim:true
    }
  }
)

//encrypting user password before saving it into User collection.
//Mongoose provide middleware(pre/post hooks) which we can use
// to manipulate our data before/after inserting into database
userschema.pre('save',function(next){
  this.password=bcrypt.hashSync(this.password,saltRounds)
  next()
})

module.exports=mongoose.model('UserInfo',userschema)
