//const { errors } = require('./constants');
class ResponseGenerator {
  sendResponse(res, data) {
    res.status(200).send( data );
  }

  sendError(res, error) {
    if (error) {
        console.log(error);
    }
    res.send(error);
  }
}

module.exports = new ResponseGenerator();
