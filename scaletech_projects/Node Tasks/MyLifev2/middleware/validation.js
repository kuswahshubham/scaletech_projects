
const express=require('express')
const bodyParser=require('body-parser')
const jwt=require('jsonwebtoken')
const app=express()
app.set('secretKey', 'nodeRestApi');


exports.validate=function (req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      res.json({status:"error", message: err.message, data:null});
    }else{
      // add user id to request
      req.body.userId = decoded.id;
      next();
    }
  });

}
