const express=require('express')
const bodyParser=require('body-parser')
const logger=require('morgan')
const db=require('./models/mylife.database')
const routes=require('./routes/mylife.route')
const adminroutes=require('./routes/mylife.admin')
const guestroutes=require('./routes/mylife.guest')
//const validateUser=require('./middleware/validation')
const validateUser=require('./middleware/Authentication')
const app=express()
let port=7000


app.set('secretKey', 'nodeRestApi');
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))
app.use('/',routes)
app.use('/admin',validateUser.verifyAdmin,adminroutes)
app.use('/guest',validateUser.verifyGuest,guestroutes)
app.use(express.static('public'));

db.connect()

app.listen(port,()=>{
  console.log("App is listening on port no.:"+port)
})
