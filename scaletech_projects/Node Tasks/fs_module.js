//non blocking behaviour of nodetutorial

var fs=require('fs');
console.log("started")

//callback concept....
fs.readFile('test_file.txt',{encoding: 'utf-8'},function(err,data){
  if(err){
    console.log('Error while reading file',err)
  }
  console.log('received data:'+data)
})
console.log("ended")

/*//blocking behaviour without callback 
var fs = require("fs");
var data = fs.readFileSync('input.txt');

console.log(data.toString());
console.log("Program Ended");
*/
