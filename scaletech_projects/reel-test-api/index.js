const http = require('http');
const req = http.request;
const HttpDispatcher=require('httpdispatcher')
const controller=require('./controllers/reel-test-controller');
const dispatcher =new HttpDispatcher()

let port='2001'
var httpserver=http.createServer(function(req,res){
  try{
    dispatcher.dispatch(req,res)
  }
  catch(err){
    console.log(err)
  }
});


//http.request('/reeldata',controller.reel_data);
dispatcher.onGet('/test',controller.test)

//below route sends 3x5 reel configuration
dispatcher.onGet("/reelconfig",controller.reel_config )

//below route expect {	"data":[[1,7,2,2,9],[2,20,7,8,1],[7,1,3,7,7]] } json object
dispatcher.onGet("/reeldata",controller.reel_data)

httpserver.listen(port)
console.log('app is listening on port:',port)

/*
var httpserver=http.createServer(function(req,res){
  res.end(JSON.stringify({"reel1":shuffle(reel1),
  "reel2":shuffle(reel2),"reel3":shuffle(reel3),"reel4":shuffle(reel4),"reel5":shuffle(reel5)}))
  //res.end()
})
*/
