//type annotation-below we intend the  greeter function to be called with single string parameter.
//it will only accepts string argument.
var Student = /** @class */ (function () {
    function Student(firstname, middlename, lastname) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.fullname = firstname + " " + middlename + " " + lastname;
    }
    return Student;
}());
//typescript allow us to implement interface just by having the shape the interface requires
function greeter(person) {
    //return "Hello"+person.firstname+" "+person.lastname;
    return person.fullname;
}
//let user={firstname:'shubham',lastname:'kuswah'}
var user = new Student("shubham", "H", "kuswah");
document.body.innerHTML = greeter(user);
