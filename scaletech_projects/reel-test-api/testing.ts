//type annotation-below we intend the  greeter function to be called with single string parameter.
//it will only accepts string argument.
class Student{
  //Classes in TypeScript are just a shorthand for
  //the same prototype-based OO that is frequently used in JavaScript
  fullname:string;
  constructor(
    public firstname:string,
    public middlename:string,
    public lastname:string
  )
  {this.fullname=firstname+" "+middlename+" "+lastname}
}

interface Person{
  firstname:string;
  lastname:string;
  fullname:string
}
//typescript allow us to implement interface just by having the shape the interface requires
function greeter(person:Person){
  //return "Hello"+person.firstname+" "+person.lastname;
  return person.fullname;
}
//let user={firstname:'shubham',lastname:'kuswah'}
let user=new Student("shubham","H","kuswah")

document.body.innerHTML =greeter(user)
