"""
1 Create a 2d matrix of variable size call it main matrix
2 Take a input of a smaller 2D matrix, smaller than main matrix
3 Check if smaller matrix is subset of main matrix

"""
#main_matrix = [[9, 4, 5],[8, 10,2] ,[1,2,3],[21,22,23]]
#sub_matrix = [[1,2,3],[21,22,23]]

rows = int(input("Enter number of rows in the matrix: "))
columns = int(input("Enter number of columns in the matrix: "))
m_matrix = []
print("Enter the %s x %s matrix: "% (rows, columns))
for i in range(rows):
    m_matrix.append(list(map(int, input().rstrip().split())))

rows = int(input("Enter number of rows in the matrix: "))
columns = int(input("Enter number of columns in the matrix: "))
s_matrix = []
print("Enter the %s x %s matrix: " % (rows, columns))
for i in range(rows):
    s_matrix.append(list(map(int, input().rstrip().split())))

#print(len(m_matrix))
#print(len(s_matrix))


if len(m_matrix) < len(s_matrix):
    print("sub matrix is  bigger than main matrix so can't check subset condition.")
else:

    # printing original Matrix
    print("Main_Matrix : " + str(m_matrix))
    print("Sub_Matrix : " +  str(s_matrix))

    # using all() to
    # check subset of list
    flag = 0
    if (all(x in m_matrix for x in s_matrix)):
        flag = 1

    # printing result
    if (flag):
        print("Yes, sub_matrix is subset of main_matrix:")
    else:
        print("No, sub_matrix is not a subset of main_matrix")
