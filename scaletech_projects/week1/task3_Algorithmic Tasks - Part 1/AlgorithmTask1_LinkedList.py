class MyNode(object):

    def __init__(self, data, next_node=None):
        self.data = data
        self.next_node = next_node

    # this method returns the stored data in node
    def get_data(self):
        return self.data

    # this method returns the next node
    def get_next(self):
        return self.next_node

    # this method to reset the pointer to new node
    def set_next(self, new_next):
        self.next_node = new_next


class MyLinkedList(object):

    def __init__(self, head=None):
        self.head = head

    def insert(self, data):
        new_node = MyNode(data)
        new_node.set_next(self.head)
        self.head = new_node
    

    def size(self):
        current = self.head
        count = 0
        while current:
            count += 1
            current = current.get_next()
        return count

    def search(self, data):
        current = self.head
        found = False
        while current and found is False:
            if current.get_data() == data:
                found = True
            else:
                current = current.get_next()
        if current is None:
            raise ValueError("Data not in list")
        return current

    def delete(self, data):
        current = self.head
        previous = None
        found = False
        while current and found is False:
            if current.get_data() == data:
                found = True
            else:
                previous = current
                current = current.get_next()
        if current is None:
            raise ValueError("Data not in list")
        if previous is None:
            self.head = current.get_next()
        else:
            previous.set_next(current.get_next())

    def display(self):
       current = self.head
       while current:
           print(current.data)
           current = current.get_next()


myList = MyLinkedList()
x=1
while x==1:
    print ("1.insert")
    print ("2.search")
    print ("3.delete")
    print ("4.display")
    print ("5.exit")

    choice=eval(input("your choice:"))

    if choice==1:
        myList.insert(5)
        myList.insert(10)
        myList.insert(15)

    elif choice==2:
        myList.search(10)
    elif choice==3:
        myList.delete(10)

    elif choice==4:
        myList.display()

    elif choice==5:
        break

    else:
        print("wrong choice")

