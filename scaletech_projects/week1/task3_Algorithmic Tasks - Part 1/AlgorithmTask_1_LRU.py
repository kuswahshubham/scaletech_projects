"""
Implement LRU cache without using any library
"""

class LRUCache(object):

    def __init__(self, capacity):

        self.capacity = capacity
        self.list = [(-1, -1)] * capacity

    def get(self, key):

        for i in range(self.capacity):
            if (key == self.list[i][0]):
                tmp = self.list.pop(i)
                self.list.append(tmp)
                return tmp[1]
        return -1

    def put(self, key, value):

        for i in range(self.capacity):
            if (key == self.list[i][0]):
                tmp = self.list.pop(i)
                self.list.append((key, value))
                return

        self.list.pop(0)
        self.list.append((key, value))
        print(self.list)
        return

cache =LRUCache(2);

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       # returns 1
cache.put(3, 3);    # evicts key 2 as the cache memory is full
cache.get(2); # returns -1 (not found)
cache.put(4, 4);    # evicts key 1
cache.get(1);       # returns -1 (not found)
cache.get(3);       # returns 3
cache.get(4);       # returns 4
